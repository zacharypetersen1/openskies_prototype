﻿using UnityEngine;
using System.Collections;



//Particle system manager for "wind" effect particles
public class WindManager : MonoBehaviour {



	GameObject epicenter;		//The center focus of the entire system, i.e particles generated around this



	//Alows all info needed for particle to be compressed into one entity
	public struct particleData {
		public int halfPeriods;		//The number of arcs in this particle's path (equal to one half of SIN period) 
		public float amplitude;		//Intensity of each arc
		public bool startPositive;	//Flags if particle will begin moving in upward arc or downward arc
		public float speed;			//Incriment that particle will be moved each frame
		public float arcSpeed;		//Incriment that arc of particle will be adjusted each frame
		public float tailDelay;		//Lifetime of tail effect
	}



	// Use this for initialization
	void Start () {
	
	}




	// Update is called once per frame
	void Update () {
	
	}



	// Generates a single wind particle
	void createParticle(particleData thisData) {

	}
}
