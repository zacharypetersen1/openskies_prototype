﻿using UnityEngine;
using System.Collections;



// A single wind particle
public class WindParticle : MonoBehaviour {



	int completedArcs = 0;	//Records how many arcs (one half of a SIN period) have already occured
	WindManager.particleData data;



	//Constructor
	public WindParticle(WindManager.particleData setData) {
		data = setData;
	}


	
	// Update is called once per frame
	void Update () {
		transform.Translate(new Vector3(data.speed, 0, 0));
	}
}
