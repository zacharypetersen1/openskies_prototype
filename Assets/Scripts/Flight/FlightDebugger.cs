﻿//Attatch to the flight debugger object

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FlightDebugger : MonoBehaviour {

    public static FlightDebugger singleton;
    public bool logEveryFrame = false;
    public type logType = type.full;
    public Color bgColor = Color.blue;
    private List<GameObject> textObjects;
    private WriteableTexture tex;
    private int resolution = 256;

    private float testAngle;

    public enum type
    {
        full, leftWing, rightWing
    }

    public enum screen
    {
        topLeft, topRight, botLeft, botRight
    }

	// Use this for initialization
	void Start () {
        //Singleton behavior
	    if(FlightDebugger.singleton != null)
        {
            Destroy(this);
        }
        else
        {
            FlightDebugger.singleton = this;
            textObjects = new List<GameObject>();
            tex = new WriteableTexture(resolution, resolution);
            gameObject.GetComponent<MeshRenderer>().material.mainTexture = tex.texture();
            draw();
        }
	}
	
    // Relay static function call to the singleton instance
    public static void log(type type)
    {
        if(FlightDebugger.singleton == null)
        {
            throw new UnityException("No instance of the flight debugger class exists.");
        }
        FlightDebugger.singleton.doLog(type);
    }

	// Update is called once per frame
	void Update () {
        if (logEveryFrame)
        {
            testAngle += Time.deltaTime;
            doLog(logType);
        }
	}

    // Draw to image and create necisary text
    private void doLog(type type)
    {
        // Reset the text list
        foreach(var textObject in textObjects)
        {
            Destroy(textObject);
        }
        textObjects.Clear();

        tex.clearBuffer(Color.black);
        drawTopLeft(0, Color.red);
        test(screen.topRight, Color.green);
        tex.apply();
   }

    // Draw a line by specifying screen, relational points, angle, and color.
    // points range from [-1, to 1] with 0 being center of specified screen.
    private void drawLineToScreen(screen s, float x1, float y1, float x2, float y2, float theta, Color color)
    {
        // Apply rotation transformation before converting relational points to pixel locations
        float x1_rot = x1 * Mathf.Cos(theta) - y1 * Mathf.Sin(theta);
        float y1_rot = x1 * Mathf.Sin(theta) + y1 * Mathf.Cos(theta);
        float x2_rot = x2 * Mathf.Cos(theta) - y2 * Mathf.Sin(theta);
        float y2_rot = x2 * Mathf.Sin(theta) + y2 * Mathf.Cos(theta);

        // Convert relational points to actual pixels in the origin screen (bottom left)
        int x_1 = (int)((resolution / 2.0f) * ((x1_rot + 1) / 2));
        int y_1 = (int)((resolution / 2.0f) * ((y1_rot + 1) / 2));
        int x_2 = (int)((resolution / 2.0f) * ((x2_rot + 1) / 2));
        int y_2 = (int)((resolution / 2.0f) * ((y2_rot + 1) / 2));

        tex.drawLine(x_1, y_1, x_2, y_2, color);

        // Draw line, apply translation to correct screen if necisary
        /*switch (s)
        {
            case (screen.topLeft):  tex.drawLine(x_1, y_1 + (int)(resolution/2), x_2, y_2 + (int)(resolution / 2), color); break;
            case (screen.topRight): tex.drawLine(x_1 + (int)(resolution / 2), y_1 + (int)(resolution / 2), x_2 + (int)(resolution / 2), y_2 + (int)(resolution / 2), color); break;
            case (screen.botLeft):  tex.drawLine(x_1, y_1, x_2, y_2, color); break;
            case (screen.botRight): tex.drawLine(x_1 + (int)(resolution / 2), y_1, x_2 + (int)(resolution / 2), y_2, color); break;
        }*/
    }

    private void drawTopLeft(float angle, Color color)
    {
        /*drawLineToScreen(screen.topLeft, -0.07f, 0.07f, 0.07f, 0.07f, angle, Color.red);
        drawLineToScreen(screen.topLeft, -0.07f, 0.07f, -0.07f, 0, angle, Color.red);
        drawLineToScreen(screen.topLeft, 0.07f, 0.07f, 0.07f, 0, angle, Color.red);
        drawLineToScreen(screen.topLeft, 0.07f, 0, 0.035f, -0.035f, angle, Color.red);
        drawLineToScreen(screen.topLeft, -0.07f, 0, -0.035f, -0.035f, angle, Color.red);
        drawLineToScreen(screen.topLeft, -0.035f, -0.07f, 0.035f, -0.07f, angle, Color.red);
        drawLineToScreen(screen.topLeft, -0.035f, -0.07f, -0.035f, -0.14f, angle, Color.red);
        drawLineToScreen(screen.topLeft, 0.035f, -0.07f, 0.035f, -0.14f, angle, Color.red);
        drawLineToScreen(screen.topLeft, -0.035f, -0.14f, 0.035f, -0.14f, angle, Color.red);
        drawLineToScreen(screen.topLeft, 0.035f, -0.14f, 0.1f, -0.28f, angle, Color.red);
        drawLineToScreen(screen.topLeft, -0.035f, -0.14f, -0.1f, -0.28f, angle, Color.red);
        drawLineToScreen(screen.topLeft, 0.1f, -0.28f, -0.1f, -0.28f, angle, Color.red);*/
    }

    private void test(screen s, Color color)
    {
        for (float angle = 0; angle < Mathf.PI * 2; angle += Mathf.PI / 10)
            drawLineToScreen(s, 0, 0, 0, 0.85f, angle, color);
    }

    // Testing
    private void draw()
    {

        // Draw grid seperating to four screens
        tex.drawLine(0, resolution / 2, resolution, resolution / 2, Color.white);
        tex.drawLine(resolution / 2, 0, resolution / 2, resolution, Color.white);
        drawTopLeft(Mathf.PI/2, Color.red);
        tex.apply();
    }



    // Sets object to location based on corelating pixel in background image 
    private void setLocation(GameObject obj, int x, int y)
    {
        obj.transform.localRotation = new Quaternion(0,0,0,0);
        obj.transform.localScale = Vector3.one;
        obj.transform.localPosition = new Vector3(-0.5f + x / (float)resolution, -0.5f + y / (float)resolution);
    }

    // Creates a text object with correct size and parent
    private GameObject makeText(string text)
    {
        GameObject temp = (GameObject) GameObject.Instantiate(Resources.Load("Debug/FDText"));
        temp.GetComponent<TextMesh>().text = text;
        temp.transform.parent = gameObject.transform;
        return temp;
    }
}
