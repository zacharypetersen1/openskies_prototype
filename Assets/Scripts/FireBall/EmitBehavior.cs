﻿using UnityEngine;
using System.Collections;

/*Name:		EmitBehavior
 *By: 		Zachary Petersen
 *Notes: 	This class is used in unison with the ObjectEmitter class. The parent obj
 *			that is being "emitted" must have this class as a component.
*/	


public class EmitBehavior : MonoBehaviour {

	[HideInInspector]
	public float lifeTime;	//The lifetime of this object
	private float deltaTime;//keeps track of how long particle has existed
	[HideInInspector]
	public bool isParent = true;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		//only check to destroy this obj if this is not the parent
		if(!isParent)
		{
			deltaTime += Time.deltaTime;
			if(deltaTime >= lifeTime)
				Destroy(gameObject);
		}
	}
}
