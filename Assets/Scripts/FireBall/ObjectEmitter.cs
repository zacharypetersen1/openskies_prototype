﻿using UnityEngine;
using System.Collections;

/*Name:		ObjectEmitter
 *By: 		Zachary Petersen
 *Notes: 	This class is similar to the the emitter in a ParticleSystem except that it
 *			emits GameObjects instead of Particles. This allows the emitted objects to
 *			have ridgedBodies and use the physics engine. Obj's should be set up with a rigidbody
 *			since this emitter will use forces to move the emitted obj's.
 *			THE OBJECT THAT IS EMITTED MUST HAVE AN EMITBEHAVIOR COMPONENT
 *Terminology: 
 *		"Flow" refers to objects that are created using the timer. After certain incriments
 *			of time, a single or multiple new obj's will be created. This generates the effect
 *			of a flow of obj's being generated over time.
 *		"Burst" refers to multiple objs being created together at once as opposed to being created
 *			seperately over time.
*/			




//Emits GameObjects much like a particle emitter emits particles
public class ObjectEmitter : MonoBehaviour 
{

	//User defined varibles that change from instance to instance
	public GameObject objType;		//the type of obj that will be emitted
	public float incriment = 1;		//incriment of time between each obj's emittence
	public float speed = 1;			//speed at which obj's are emitted
	public float lifetime = 1;		//lifetime of the obj's that are emitted
	public float angleVarience = 0;	//the amount of random varience there will be in the emitted obj's direction of movement


	//Variables that are required for class functionality
	private float deltaTime;		//measures amount of time that has passed
	private GameObject nextObj;		//used as placeholder to access each obj since they are created one at a time
	private EmitBehavior nextEmit;	//used to access the EmitBehavior attached to each obj when it is created
	private float varienceNum;		//used to create angle varience (range is [-1,1] )
	[HideInInspector]
	public bool isFlowing = true;	//used to control if a flow of obj's are being emitted


	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		//Call flow if this instance is set to flow
		//if(isFlowing == true)
			emitFlow();
	}

	//Used to emit a burst of objects
	public void emitBurst(int amount)
	{

	}

	//Usied to update and emit flow of objects
	void emitFlow()
	{
		//Update the change in time
		deltaTime += Time.deltaTime;

		//Check if we have passed our time incriment between obj's. If so, keep
		//looping this block until we have taken care of each incriment
		while(deltaTime >= incriment)
		{
			//Create obj and set var's in EmitBehavior
			nextObj = (GameObject) Instantiate(objType);
			nextEmit = nextObj.GetComponent<EmitBehavior>();
			nextEmit.lifeTime = lifetime;
			nextEmit.isParent = false;
			//Set the position and rotation of the obj
			nextObj.transform.position = transform.position;
			nextObj.transform.eulerAngles = transform.eulerAngles;
			varienceNum = Random.Range(0,2*Mathf.PI);
			nextObj.transform.Rotate(new Vector3(Mathf.Sin(varienceNum)*angleVarience, Mathf.Cos(varienceNum)*angleVarience));
			//shoot the object out
			nextObj.GetComponent<Rigidbody>().AddForce(nextObj.transform.forward*speed);

			//Reduce deltaTime by incriment
			deltaTime -= incriment;

		}

	}
}
