﻿using UnityEngine;
using System.Collections;

//Maintains fireball objects
public class FireBall : MonoBehaviour {

	public float speed = 150f;
	public float maxLifeTime = 7;
	float lifeTime = 0;

	void Update () {
		//Move fireball forward, destroy itself after lifetime is up
		transform.Translate(Vector3.forward*speed*Time.deltaTime);
		lifeTime += Time.deltaTime;
		if(lifeTime > maxLifeTime)
			Destroy(gameObject);
	}


	void OnTriggerEnter(Collider other) {
		switch(other.tag) {
		case "building":
			//Set the building on fire then destroy self
			FireListener temp = other.GetComponent<FireListener>();
			temp.fire.GetComponent<ParticleSystem>().enableEmission = true;
			temp.smoke.GetComponent<ParticleSystem>().enableEmission = true;
			Destroy(gameObject);
			break;
		case "terrain":
			//Destroy self
			Destroy(gameObject);
			break;
		}
	}
}
