﻿using UnityEngine;
using System.Collections;

//Stores prefabs for easy access using code
public class PrefabStore : MonoBehaviour {

	public GameObject _fireBall, _fireSpray;
	public static GameObject fireBall, fireSpray;

	void Start () {
		fireBall = _fireBall;
		fireSpray = _fireSpray;
	}
}
