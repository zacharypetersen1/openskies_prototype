﻿using UnityEngine;

class SignedUnitPoint
{
    private float ptx;
    private float pty;

    public SignedUnitPoint(float x, float y)
    {
        ptx = x;
        pty = y;
    }

    // rotates this point around (0, 0) by specified angle
    private void rotate(float angle)
    {
        float tmpx = ptx;
        float tmpy = pty;
        ptx = tmpx * Mathf.Cos(angle) - tmpy * Mathf.Sin(angle);
        pty = tmpx * Mathf.Sin(angle) + tmpy * Mathf.Cos(angle);
    }

    // normalizes x and y components of this point between [0,1]
    // value of -1 becomes 0. 
    // value of  0 becomes 0.5 
    // value of  1 becomes 1 
    private void normalize()
    {
        ptx = (ptx + 1) / 2;
        pty = (pty + 1) / 2;
    }


}
