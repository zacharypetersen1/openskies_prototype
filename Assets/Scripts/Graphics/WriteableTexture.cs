﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class WriteableTexture {

    private Color[] buffer;
    private Texture2D tex;
    private readonly int width, height;

    // Constructor
	public WriteableTexture(int setWidth, int setHeight)
    {
        width = setWidth;
        height = setHeight;
        buffer = Enumerable.Repeat<Color>(Color.black, width * height).ToArray<Color>();
        tex = new Texture2D(width, height);
        tex.filterMode = FilterMode.Point;
    }

    // Sets every pixel in buffer to specified color
    public void clearBuffer(Color color)
    {
        for (int i = 0; i < buffer.Length; i++)
        {
            buffer[i] = color;
        }
    }

    // Sets color of pixel in the buffer
    public void setPixel(int x, int y, Color color)
    {
        if (x > 0 && x < width - 1 && y > 0 && y < height - 1)
        {
            buffer[y * width + x] = color;
        }
    }

    // Draws a line in the buffer using Bresenham's algorithm
    public void drawLine(int x1, int y1, int x2, int y2, Color color)
    {
        int deltaX = x2 - x1;
        int deltaY = y2 - y1;

        //Check if vertical or horizontal
        if(deltaY == 0)
        {
            for(int i = Mathf.Min(x1, x2); i <= Mathf.Max(x1, x2); i++){
                setPixel(i, y1, color);
            }
            return;
        }
        if (deltaX == 0)
        {
            for (int i = Mathf.Min(y1, y2); i <= Mathf.Max(y1, y2); i++)
            {
                setPixel(x1, i, color);
            }
            return;
        }

        if (Mathf.Abs(deltaY) > Mathf.Abs(deltaX))
        {
            //execute the algorithm
            float error = 0;
            float deltaError = Mathf.Abs(deltaY / (float)deltaX);
            int y = y1;

            for (int x = x1; Mathf.Abs(x1 - x) <= Mathf.Abs(x1 - x2); x += (int)Mathf.Sign(x2 - x1))
            {
                error += deltaError;
                while (error >= 0.5f)
                {
                    y += (int)Mathf.Sign(y2 - y1);
                    error -= 1.0f;
                    setPixel(x, y, color);
                }
            }
        }
        else
        {
            //execute the algorithm
            float error = 0;
            float deltaError = Mathf.Abs(deltaX / (float)deltaY);
            int x = x1;

            for (int y = y1; Mathf.Abs(y1 - y) <= Mathf.Abs(y1 - y2); y += (int)Mathf.Sign(y2 - y1))
            {
                error += deltaError;
                while (error >= 0.5f)
                {
                    x += (int)Mathf.Sign(x2 - x1);
                    error -= 1.0f;
                    setPixel(x, y, color);
                }
            }
        }
    }

    // Draws a rectangle to the buffer
    public void drawRect(int x, int y, int width, int height, Color color)
    {
        drawLine(x, y, x + width, y, color);
        drawLine(x, y, x, y+height, color);
        drawLine(x+width, y, x + width, y+height, color);
        drawLine(x, y+height, x + width, y+height, color);
    }

    // Applies changes in the buffer to the texture
    public void apply()
    {
        tex.SetPixels(buffer);
        tex.Apply();
    }

    // Getter for the Texture2D field
    public Texture2D texture()
    {
        return tex;
    }
}
