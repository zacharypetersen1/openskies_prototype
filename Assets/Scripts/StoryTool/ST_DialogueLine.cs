﻿using UnityEngine;
using System.Collections;
using System.Text;

public class ST_DialogueLine : MonoBehaviour {

    private TextMesh text_mesh;
    private ST_TextData text_data = new ST_TextData("Hmmm... I wonder what I should do today.");
    private StringBuilder body = new StringBuilder("<size=100><color=#00000000>|</color></size>");
    private StringBuilder front_letter = new StringBuilder("<size=070><color=#000000FF></color></size>");
    private float time = 0;
    private float speed = 1f;
    private float appear_time = .05f;
    private float delay_regular = 0f;
    private float delay_punctuation = .3f;
    private float time_bound;
    private state cur_state = state.appear;

    private enum state
    {
        appear, delay
    }

	// Use this for initialization
	void Start () {
        time_bound = appear_time;
        text_mesh = gameObject.GetComponent<TextMesh>();
        front_letter.Insert(27, text_data.getNextChar());
        transform.localScale = Vector3.one * 0.035f;
    }

    void Update ()
    {

    }

    // Updates the appearence of the dialogue line
    public bool manualUpdate (float deltaTime)
    {
        time += deltaTime;
        while (time > time_bound)
        {
            time -= time_bound;
            flipState();

            // If state is delay, update what is needed to exit from 'appear' state
            if (cur_state == state.delay)
            {
                // Get the next character. Make sure it is not a space
                char tmp_char;
                do {
                    tmp_char = text_data.getNextChar();
                    addCharacter(tmp_char);
                } while (tmp_char == ' ');

                // check if the whole line has been displayed
                if (tmp_char == '\0')
                {
                    stopUpdate();
                    return true;
                }
                setLeadCharAlpha(0);
            }
        }


        if (cur_state == state.appear)
        {
            setLeadCharAlpha(time / time_bound);
            setLeadCharSize(sizeFunction(time / time_bound));
        }

        return false;
    }

    // Appends a letter onto the existing dialogue.
    public void addCharacter (char character)
    {
        char tmp = front_letter[27];
        front_letter.Remove(27, 1);
        front_letter.Insert(27, character);
        body.Append(tmp);
        updateText();
    }

    // Updates the text in the text-renderer component
    private void updateText()
    {
        text_mesh.text = body.ToString() + front_letter.ToString();
    }

    // Sets the color of the front letter in the dialogue line
    private void setLeadCharRBG (Color color)
    {
        #if DEBUG_VER
        // Check if user might be expecting alpha to have effect
        if (color.a != 1)
            Debug.LogWarning("the alpha field of this color argument: " +
                color.ToString() + " will be ingored when setting color of dialogue line");
        #endif

        // Convert rgb color to hex
        string hex = ((int)(color.r * 255)).ToString("X2") +
            ((int)(color.g * 255)).ToString("X2") +
            ((int)(color.b * 255)).ToString("X2");
        Debug.Log(hex);

        // Set the new color inside of the dialogue line
        front_letter.Remove(18, 6);
        front_letter.Insert(18, hex);

        updateText();
    }

    // Sets the visibility of the front letter in the dialogue line
    private void setLeadCharAlpha (float value)
    {
        // Check if argument is not within valid range
        if ((value < 0) || (value > 1))
            throw new UnityException("Argument out of bounds [0,1]: " + value.ToString());

        // Convert alpha value to hex
        string hex = ((int)(value * 255)).ToString("X2");

        // Set the new alpha value
        front_letter.Remove(24, 2);
        front_letter.Insert(24, hex);

        updateText();
    }

    // Sets the size of the leading character
    public void setLeadCharSize (int size)
    {
        // Check if size is out of valid range
        if ((size <= 0) || (size > 999))
            throw new UnityException("argument 'size' (" +
                size.ToString() + ") must be in range [1, 999]");

        // Insert the new size
        front_letter.Remove(6, 3);
        front_letter.Insert(6, size.ToString("D3"));

        updateText();
    }

    // Sets the state and associated values
    private void flipState()
    {
        cur_state = cur_state == state.appear ? state.delay : state.appear;
        time_bound = cur_state == state.appear ? appear_time * (1 / speed) 
            : getDelayTime(front_letter[27]) * (1 / speed);
    }

    // Gets the ammount of time that should be used for delay after a character
    private float getDelayTime(char character)
    {
        if ((character == '.') || (character == ',') || (character == '?') || (character == '!'))
            return delay_punctuation;
        else
            return delay_regular;
    }

    // 
    private int sizeFunction (float normalized_time)
    {
        if (normalized_time < .7f)
        {
            return (int)(80 * (normalized_time / .7f));
        }

        else
            return (int)(80 - 30 * ((normalized_time - .7f) / .3f));
    }

    //Terminates the update cycle
    private void stopUpdate()
    {
        text_mesh.text = body.ToString();
    }
}
