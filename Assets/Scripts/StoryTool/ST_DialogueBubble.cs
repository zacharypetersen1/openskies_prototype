﻿using UnityEngine;
using System.Collections;

public class ST_DialogueBubble : MonoBehaviour {

    private int num_slots;
    private ST_DialogueLine[] slots;
    private int active_slot = 0;
    private bool done_updating = false;

	// Use this for initialization
	void Start () {
        slots = new ST_DialogueLine[3];
        slots[0] = ((GameObject)(Instantiate(Resources.Load("DialogueLine")))).GetComponent<ST_DialogueLine>();
        slots[0].gameObject.GetComponent<RectTransform>().parent = gameObject.transform;
        slots[0].gameObject.GetComponent<RectTransform>().position = transform.position;
        slots[0].transform.localScale = transform.localScale;
        slots[0].transform.localPosition = new Vector3(-1.7f, 0, 0);
    }
	
	// Update is called once per frame
	void Update () {
	    if (!done_updating)
        {
            if(slots[active_slot].manualUpdate(Time.deltaTime))
            {
                done_updating = true;
            }
        }
	}
}
