﻿using UnityEngine;
using System.Collections;

public class ST_TextData
{
    private const char RULE = '`';
    private string text;
    private int text_index = 0;
    private int rule_index = 0;
    private int[][] rules;
    private int index = 0;

    //Constructor
    public ST_TextData (string set_text)
    {
        text = set_text;
    }

    //Returns next char in the sequence
    public char getNextChar ()
    {
        if (index == text.Length)
            return '\0';
        else
            return text[index++];
    }
}
