﻿using UnityEngine;
using System.Collections;

//Used to light buildings on fire
//Will be accessed by the FireBall class
public class FireListener : MonoBehaviour {

	public GameObject smoke, fire;

	void Start () {
		fire.GetComponent<ParticleSystem>().enableEmission = false;
		smoke.GetComponent<ParticleSystem>().enableEmission = false;
	}

}