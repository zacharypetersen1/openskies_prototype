﻿using UnityEngine;
using System.Collections;

//
//Handles game state
//
public class Master : MonoBehaviour {

	Pilot thisPilot;
	Paused thisGrounded;
	Canvas pauseMenu, pauseHint, targetReticule;

	void Start () {
		//targetReticule = GameObject.Find("TargetReticule").GetComponent<Canvas>();
		//thisPilot = gameObject.GetComponent<Pilot>();
		//thisPilot.enabled = false;
		//thisGrounded = gameObject.GetComponent<Paused>();
		//targetReticule.enabled = false;
		//pauseHint.enabled = false;
	}
	
	void Update () {

		//
		//When space is pressed, switch to 'play' mode
		//
		if(Input.GetKeyDown(KeyCode.Space))
		{
			thisPilot.enabled = true;
			thisGrounded.enabled = false;
			pauseMenu.enabled = false;
			targetReticule.enabled = true;
			pauseHint.enabled = true;
		}

		//
		//When ESC is pressed, switch to 'pause' mode
		//
		/*if(Input.GetKeyDown(KeyCode.Escape))
		{
			thisPilot.enabled = false;
			thisGrounded.enabled = true;
			pauseMenu.enabled = true;
			targetReticule.enabled = false;
			pauseHint.enabled = false;
		}*/
	}
}
