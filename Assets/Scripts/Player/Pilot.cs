﻿ using UnityEngine;
using System.Collections;

//
//Handles player flight and controls
//
public class Pilot : MonoBehaviour {

	Transform camTrans, cameraTarget;
	Transform rightWing, leftWing;
	float forwardFlap = 10f; 
	float backwardFlap = 6f;
	float upwardFlap = 5000f;			//TODO scale this according to speed
	//[HideInInspector]
	public float forwardSpeed = 0;		//Grabbed by FollowCam
	float gravity = 10f;
	float flapVertRotation = 0;
	float flapFlapRotation;
	public float airFriction = 0;
	bool isFlap;
	GameObject fireBall, fireSpray;
	FireBall fireBallScript;
	FireSpray fireSprayScript;
	ParticleSystem fireSprayParticle;
	bool isRecovering = false;
	Vector3 recoveryPos;

	// Use this for initialization
	void Start () {
		camTrans = GameObject.Find ("MainCam").GetComponent<Transform>();
		rightWing = GameObject.Find("wingRight").GetComponent<Transform>();
		leftWing = GameObject.Find("wingLeft").GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {

		//
		//If state is "recovering from crash", move directly toward the recovery position
		//
		if(isRecovering) {
			transform.position = Vector3.MoveTowards(transform.position, recoveryPos, .2f);
			if(transform.position == recoveryPos)
				isRecovering = false;
			return;
		}

		//
		//Player Movement
		//TODO holding forward decreases airfriction
		//TODO adjust friction for neutral
		//TODO set up system to use GetButtonDown() instead of GetKeyDown()
		//

		//
		//Get air friction
		//
		if(!Input.GetKey(KeyCode.L))
			airFriction = forwardSpeed/2000;
		else
			airFriction = forwardSpeed/100;

		//
		//Update speed and position
		//
		forwardSpeed -= airFriction;
		forwardSpeed -= transform.forward.y * Time.deltaTime * 10;
		if(forwardSpeed < 0)
			forwardSpeed = 0;
		transform.position += transform.forward * Time.deltaTime * forwardSpeed;

		//
		//Handle rotation
		//
		transform.Rotate(Vector3.right * Input.GetAxis("Vertical") * Time.deltaTime * 90);
		transform.Rotate(-Vector3.forward*Input.GetAxis("Horizontal") * Time.deltaTime * 90);
		transform.Rotate(Vector3.up * Input.GetAxis("flapHoriz") * Time.deltaTime * 30);

		//
		//Handle "Flap" by listening for input and executing "Flap" when key is pressed
		//
		if(Input.GetKeyDown(KeyCode.Space))
		{
			//
			//3 types of flaps, forward, backward, and neutral
			//
			if(Input.GetKey(KeyCode.O))			//Forward
			{
				GetComponent<Rigidbody>().AddForce(transform.up * upwardFlap/2);
				forwardSpeed += forwardFlap;
			}
			else if(Input.GetKey(KeyCode.L))	//Backward
			{
				forwardSpeed -= backwardFlap*4;
				GetComponent<Rigidbody>().AddForce(transform.up * upwardFlap);
			}
			else								//Neutral
				GetComponent<Rigidbody>().AddForce(transform.up * upwardFlap);
			
			isFlap = true;
		}

		//
		//Factor speed and gravity variables into velocity of character's rigidbody
		//
		GetComponent<Rigidbody>().velocity = Vector3.Lerp(GetComponent<Rigidbody>().velocity, transform.forward*forwardSpeed, .05f);
		GetComponent<Rigidbody>().AddForce(Vector3.down * gravity);

		//
		//Create fireball if E key was pressed
		//
		if(Input.GetKeyDown(KeyCode.E))
			makeFireball();

		//
		//Run function that handles wing animation
		//
		flapAnim();
	}



	//
	//Collision function- handles collisions with all other objects
	//
	void OnTriggerEnter(Collider other) {

		//
		//Check tag of other object
		//
		switch (other.tag) {
		case "building":
		case "terrain":
			//
			//Crash has occured! Calculate recovery position and turn on isRecovering
			//
			GetComponent<Rigidbody>().velocity = Vector3.zero;
			forwardSpeed = 0;
			isRecovering = true;
			recoveryPos = transform.position + Vector3.up*20;
			break;
		}
	}



	//
	//Handles animation for wings
	//
	void flapAnim()
	{
		flapVertRotation = Input.GetAxis("flapVert")*50;
		rightWing.eulerAngles = transform.eulerAngles;
		leftWing.eulerAngles = transform.eulerAngles;
		rightWing.Rotate(Vector3.right * flapVertRotation);
		leftWing.Rotate(Vector3.right * flapVertRotation);

		if(isFlap)
		{
			flapFlapRotation = Mathf.Lerp(flapFlapRotation, 50, .3f);
			if(flapFlapRotation >= 49)
				isFlap = false;
		}
		else
		{
			flapFlapRotation = Mathf.Lerp(flapFlapRotation, 0, .15f);
		}

		rightWing.Rotate(Vector3.back * flapFlapRotation);
		leftWing.Rotate(Vector3.forward * flapFlapRotation);
	}

	

	//
	//Creates fireball object
	//
	void makeFireball()
	{
		//
		//Create fireBall
		//
		fireBall = (GameObject) Instantiate(PrefabStore.fireBall);
		fireBall.transform.eulerAngles = transform.eulerAngles;
		fireBall.transform.position = transform.position;
		fireBallScript = fireBall.GetComponent<FireBall>();
		fireBallScript.speed = fireBallScript.speed+forwardSpeed*2;

		//
		//Create fireSpray
		//
		fireSpray = (GameObject) Instantiate(PrefabStore.fireSpray);
		fireSpray.transform.eulerAngles = transform.eulerAngles;
		fireSpray.transform.position = transform.position;
		fireSpray.transform.Translate(Vector3.forward*1);

		//
		//Adjust particle system of fireSpray object
		//
		fireSprayParticle = fireSpray.GetComponent<ParticleSystem>();
		fireSprayParticle.startSpeed = 60+forwardSpeed*2;
		fireSprayParticle.Emit(50);
	}
}
