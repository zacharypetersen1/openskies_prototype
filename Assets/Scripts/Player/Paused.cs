﻿using UnityEngine;
using System.Collections;

//
//Temporary Class that keeps the player stationary while in "pause mode"
//
public class Paused : MonoBehaviour {

	float rotX, rotY, rotZ;

	void Update ()
	{
		GetComponent<Rigidbody>().velocity = Vector3.zero;

		rotX = transform.eulerAngles.x;
		rotZ = transform.eulerAngles.z;
		rotY = transform.eulerAngles.y;
		transform.eulerAngles = new Vector3(rotX,rotY,rotZ);
	}
}
