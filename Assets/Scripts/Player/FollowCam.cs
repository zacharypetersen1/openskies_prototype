﻿using UnityEngine;
using System.Collections;


//TODO add camera shake
//TODO add left/right adjustments
//TODO add up/down adjustments
//Updates the main camera to follow the player
public class FollowCam : MonoBehaviour {



	Pilot pilot;					//Used to access pilot class on player
	float forwardSpeed;				//Taken from the pilot class
	float speedCap = 80;			//Speed will not effect the camera after passing this cap
	Transform charTrans;			//Used to get position of player
	Vector3 currentOffset;			//Actual current offset of camera from the player
	float shakeScalar;				//Stores scalar that is passed into calculate function of shake object
	Offsetter forwardOffsetter;
	Offsetter LROffsetter;			//Offsetters and ConstantShake abstract camera manipulation to seperate classes
	Offsetter upDownOffsetter;
	ConstantShake shake;



	//
	//Runs once when first created
	//
	void Start () {
		pilot = GameObject.Find("Player").GetComponent<Pilot>();
		charTrans = GameObject.Find("Player").GetComponent<Transform>();
		forwardOffsetter = new Offsetter(new Vector3(-2, 0.6f, 0), 10, 1.6f);
		LROffsetter = new Offsetter(new Vector3(0, 0, 1), 10, 1.5f, true, Vector3.zero, 40);
		upDownOffsetter = new Offsetter(new Vector3(0, -1.5f, 0), new Vector3(0, .3f, 0), 10, 1.2f, true, Vector3.zero, 40);
		shake = new ConstantShake(0);

		//
		//Get the parameters for the constantShake object from the external Options object
		//
		if(!GameObject.Find("Options").GetComponent<Options>().motionBob)
			shake.setShakeSize(0);
		else
			shake.setShakeSize(
				GameObject.Find("Options").GetComponent<Options>().motionBobSize / 100 );
	}



	//
	//Runs every frame
	//
	void Update () {
		//
		//FOR TESTING IN UNITY EDITOR ONLY
		//Query the 'Options' opject to see if options were updated in the editor window
		//
		if(!GameObject.Find("Options").GetComponent<Options>().motionBob)
			shake.setShakeSize(0);
		else
			shake.setShakeSize(
				GameObject.Find("Options").GetComponent<Options>().motionBobSize / 100 );

		//
		//Collect information from the player
		//
		forwardSpeed = pilot.forwardSpeed;

		//
		//Normalize information to 0-1 scale
		//
		if(forwardSpeed < 0)
			forwardSpeed = 0;
		if(forwardSpeed > speedCap)
			forwardSpeed = speedCap;
		forwardSpeed = 1 - (forwardSpeed/speedCap);

		//
		//Update the various offsetters
		//
		forwardOffsetter.calculate(forwardSpeed);
		LROffsetter.calculate(forwardSpeed, Input.GetKey(KeyCode.K), Input.GetKey(KeyCode.Semicolon));
		upDownOffsetter.calculate(forwardSpeed, Input.GetKey(KeyCode.S), Input.GetKey(KeyCode.W));
		shakeScalar = 1-forwardSpeed;
		if(shakeScalar < 0.5f)
			shakeScalar = 0;
		else
			shakeScalar = (shakeScalar - 0.5f) * 2;
		shake.calculate(shakeScalar);

		//
		//Calculate the current camera offset using the offsetters
		//
		currentOffset = forwardOffsetter.offset() + LROffsetter.offset() + upDownOffsetter.offset() + shake.offset();

		//
		//set Camera using currentOffset
		//
		transform.position = charTrans.position 
							 + charTrans.forward * currentOffset.x
							 + charTrans.up      * currentOffset.y
						     + charTrans.right	 * currentOffset.z;
		transform.rotation = charTrans.rotation;
		transform.Rotate(shake.offset()*20);
	}



	//
	// offsets camera, can be used in two ways:
	//							      one direction  :  closestPos <--> furthestPos
	//							      two directions :  -furthestPos <--> -closestPos <--> ZeroPosition <--> closestPos <--> furthestPos
	//
	private class Offsetter {
	
		Vector3 basePosistion;	//Base position of positive offset
		Vector3 negBasePosition;//Base position of negative offset
		float maxScalar;		//Amount of scale applied to base position to obtain max position
		float zoomStep;			//Step size- controls speed that camera movement from this offset
		Vector3 targetOffset;	//Target offset position
		Vector3 actualOffset;	//Actual offset position
		Vector3 zeroPosition = Vector3.zero;	//zero position (only for two direction offsets)
		bool pastActivity = false;	//Records activity (usu. key press) of last frame (used for slow out of the offset)
		int slowOutMax;			//Total number of frames in the slow out
		int slowOutCount;		//Current frame of the slow out

		//
		//Basic one direction constructor
		//
		public Offsetter(Vector3 setBasePosition, float setMaxScalar, float setZoomStep): 
						 this(setBasePosition, setMaxScalar, setZoomStep, false, Vector3.zero, 1) {}

		//
		//Symetric two direction constructor
		//
		public Offsetter(Vector3 setBasePosition, float setMaxScalar, float setZoomStep, bool startAtZero, Vector3 setZeroPosition, int setSlowOutMax): 
						 this(setBasePosition, -setBasePosition, setMaxScalar, setZoomStep, startAtZero, setZeroPosition, setSlowOutMax) {}

		//
		//Asymetric two direction constructor
		//
		public Offsetter(Vector3 setBasePosition, Vector3 setNegBasePosition, float setMaxScalar, float setZoomStep, bool startAtZero, Vector3 setZeroPosition, int setSlowOutMax) {
			basePosistion = setBasePosition;
			negBasePosition = setNegBasePosition;
			maxScalar = setMaxScalar;
			zoomStep = setZoomStep;
			zeroPosition = setZeroPosition;
			if(startAtZero)
				actualOffset = zeroPosition;
			else actualOffset = basePosistion;
			slowOutMax = setSlowOutMax;
			if(slowOutMax < 1)
				slowOutMax = 1;
		}

		//
		//version for "one possible direction" calculation
		//
		public void calculate(float scalar) {
			targetOffset = basePosistion + (basePosistion*maxScalar)*scalar;
			float thisZoomStep = (zoomStep+(zoomStep*maxScalar)*scalar)*Time.deltaTime;
			actualOffset = Vector3.MoveTowards(actualOffset, targetOffset, thisZoomStep);
		}

		//
		//version for "two possible direction" calculation
		//Bools negative and positive are usually representative of buttons being pressed
		//
		public void calculate(float scalar, bool negative, bool positive) {

			//
			//Find target offset position
			//
			if(negative != positive) {
				if(negative)
					targetOffset = negBasePosition + (negBasePosition*maxScalar)*scalar;
				else
					targetOffset = basePosistion + (basePosistion*maxScalar)*scalar;
			}
			else targetOffset = zeroPosition;	//if both or no buttons are pressed, offset moves towards zero position

			//
			//Determine activity state and check if there is a state change
			//
			bool currentActivity = negative ^ positive;
			bool activityStateChange = pastActivity ^ currentActivity;
			pastActivity = currentActivity;

			//
			//Determine if it necisarry to reset the slow out calculation
			//
			bool reset = false;
			if(activityStateChange) {
				reset = true;
			}									

			float thisZoomStep = (zoomStep+(zoomStep*maxScalar)*scalar)*Time.deltaTime;
			thisZoomStep *= getSlowOutScalar(reset);
			actualOffset = Vector3.MoveTowards(actualOffset, targetOffset, thisZoomStep);
		}

		//
		//Calculates the value of the slow out scalar (used to create a smooth acceleration of offset speed)
		//
		private float getSlowOutScalar(bool reset) {

			if(reset)
				slowOutCount = 0;

			//
			//Calculate slow out scalar
			//
			if(slowOutCount < slowOutMax)
				slowOutCount++;
			return (float)slowOutCount/slowOutMax;
		}

		//
		//Accessor for actualOffset field
		//
		public Vector3 offset() {
			return actualOffset;
		}
	}



	//
	//Simulates the camera shaking
	//
	class ConstantShake{
		float time = 0;			//Will be plugged into trig functions to determine location on trig curve
		float angle = 0.5f;		//Direction of the shake as an angle
		float magnitudeScalar;	//Magnitude of the peak of the current shake
		float shakeSize;		//Magnitude of the largest possible shake
		Vector3 offsetVec;		//Stores the offset vector generated by the shake calculation



		//
		//Constructor - setSize is the size of the largest possible shake
		//
		public ConstantShake(float setSize) {
			shakeSize = setSize;
			generateMagScalar();
		}



		//
		//Calculates the offset vector of the shake this frame
		//
		public void calculate(float externalScalar) {

			//
			//Update time since last frame
			//
			time += Time.deltaTime * 30;
			if(time > Mathf.PI*2) {
				time -= Mathf.PI*2;
				generateMagScalar();
			}

			//
			//Calculate the current magnitude of the offset vector by factoring the position on sin graph, magnitude scalar, size of this shakeobject, and the external scalar
			//
			float currentMagnitude = Mathf.Sin(time) * magnitudeScalar * externalScalar * shakeSize;

			//
			//Calculate offset vector using angle and magnitude
			//
			offsetVec = new Vector3(0, Mathf.Sin(angle)*currentMagnitude, Mathf.Cos(angle)*currentMagnitude);
		}



		//
		//Calculates a new magnitude scalar
		//
		private void generateMagScalar() {
			//TODO increase complexity of magnitude generation to get more realistic feeling shakes
			if(Random.Range(0,1f) < 0.8f)
				magnitudeScalar = Random.Range(0.2f, 0.5f);
			else
				magnitudeScalar = Random.Range(0.7f, 1f);

			angle += Random.Range(-0.1f, 0.14f);
		}



		//
		//Mutator for shakeSize field
		//
		public void setShakeSize(float setToThis) {
			shakeSize = setToThis;
		}



		//
		//Accessor for the offsetVec field
		//
		public Vector3 offset() {
			return offsetVec;
		}
	}
}
