﻿using UnityEngine;
using System.Collections;

//
//Allows developer to easily adjust settings in game
//
public class Options : MonoBehaviour {
	public bool motionBob = false;		//Determines if motion bob is turned on or off
	public float motionBobSize = 2.5f;	//Magnitude of the motion bob
}
